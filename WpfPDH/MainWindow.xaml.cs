﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace WpfPDH
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Path to DLL
        /// </summary>
        private const string DllFilePath = @"C:\Users\BUROVIC\source\repos\problemdeshandlungsreisenden\Debug\ProblemDesHandlungsreisenden.dll";
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        private extern static void FindRoute(int N, int[] X, int[] Y);
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        private extern static int GetCurrentIterationNum();

        public ObservableCollection<Coordinate> Towns { get; set; } = new ObservableCollection<Coordinate>();

        static Random rnd = new Random();

        public MainWindow()
        {
            InitializeComponent();
        }

        public bool IsValid(DependencyObject parent)
        {
            if (Validation.GetHasError(parent))
                return false;

            // Validate all the bindings on the children
            for (int i = 0; i != VisualTreeHelper.GetChildrenCount(parent); ++i)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                if (!IsValid(child)) { return false; }
            }

            return true;
        }

        private void FindRouteClick(object sender, RoutedEventArgs e)
        {

            if (!IsValid(CoordinatesDataGrid))
            {
                MessageBox.Show("Datagrid validation error",
                    "Validation error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            int N = Towns.Count;

            if (N < 2 || N > 15)
            {
                return;
            }

            ProgressBarWindow progressBarWindow = new ProgressBarWindow();
            progressBarWindow.Show();

            var task = Task.Factory.StartNew(() => {
                FindRoute(N, Towns.Select(t => Convert.ToInt32(t.X)).ToArray(), Towns.Select(t => Convert.ToInt32(t.Y)).ToArray());
            });

            Task.Factory.StartNew(() =>
            {
                while (!task.IsCompleted)
                {
                    Dispatcher.Invoke(() =>
                    {
                        progressBarWindow.Value = ((double)GetCurrentIterationNum() / Factorial(N)) * 100;
                        Thread.Sleep(100);
                    });
                }

                Dispatcher.Invoke(() =>
                {
                    progressBarWindow.Close();

                    PixelImageViewerWindow pixelImageViewer = new PixelImageViewerWindow(Directory.GetCurrentDirectory() + "\\picture.png");
                    pixelImageViewer.Show();
                });
            });
        }

        public int Factorial(int numb)
        {
            int res = 1;
            for (int i = numb; i > 1; i--)
                res *= i;
            return res;
        }

        private void CoordinatesDataGrid_Loaded(object sender, RoutedEventArgs e)
        {

            CoordinatesDataGrid.CanUserAddRows = true;
            CoordinatesDataGrid.CanUserDeleteRows = true;

            CoordinatesDataGrid.DataContext = Towns;
            for (int i = 0; i < rnd.Next(9, 9); i++)
            {
                Towns.Add(new Coordinate(rnd.Next(-100, 100), rnd.Next(-100, 100)));
            }


            CoordinatesDataGrid.ItemsSource = Towns;
        }

        private void RemoveRow(object sender, RoutedEventArgs e)
        {
            try
            {
                Towns.RemoveAt(CoordinatesDataGrid.SelectedIndex);
            }
            catch (Exception){ }
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Xml documents (.xml)|*.xml"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = dlg.FileName;

                var x = new XmlSerializer(typeof(ObservableCollection<Coordinate>));
                var fs = new FileStream(filename, FileMode.OpenOrCreate);
                x.Serialize(fs, Towns);
                fs.Close();
            }
        }

        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Xml documents (.xml)|*.xml"; 

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                // Save document
                string filename = dlg.FileName;

                var x = new XmlSerializer(typeof(ObservableCollection<Coordinate>));
                var fs = new FileStream(filename, FileMode.Open);
                Towns = x.Deserialize(fs) as ObservableCollection<Coordinate>;
                fs.Close();

                CoordinatesDataGrid.ItemsSource = null;
                CoordinatesDataGrid.ItemsSource = Towns;
            }
        }
    }
}
