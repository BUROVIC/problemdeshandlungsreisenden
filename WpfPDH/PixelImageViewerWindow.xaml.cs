﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfPDH
{
    /// <summary>
    /// Interaction logic for PixelImageViewerWindow.xaml
    /// </summary>
    public partial class PixelImageViewerWindow : Window
    {
        public PixelImageViewerWindow(string path)
        {
            InitializeComponent();

            MainImage.Source = new BitmapImage(new Uri(path));
        }

    }
}
