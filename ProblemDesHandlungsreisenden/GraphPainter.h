#pragma once

#include "allincluded.h"
#include <fstream>

using namespace std;

class GraphPainter
{
public:
    GraphPainter(vector<Coordinate> map);
    ~GraphPainter();

    void MakeRoute(vector<Coordinate> map, vector<int> route);

    void Draw();

private:

    int imageWidth;
    int imageHeight;

    PixelColor** imageData;
    
    //���������� �� ��������� 
    Coordinate offset;

    void DrawLine(Coordinate origin, Coordinate endpoint);
};

