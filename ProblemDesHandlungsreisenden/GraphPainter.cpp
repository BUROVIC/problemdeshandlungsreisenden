#include "pch.h"
#include "GraphPainter.h"


GraphPainter::GraphPainter(vector<Coordinate> map)
{
    Coordinate max = map[0];
    Coordinate min = map[0];;
    for (auto town: map)
    {
        if (town.X > max.X)
            max.X = town.X;
        if (town.Y > max.Y)
            max.Y = town.Y;
        if (town.X < min.X)
            min.X = town.X;
        if (town.Y < min.Y)
            min.Y = town.Y;
    }

    int borderSize = 10;
    max.X += borderSize;
    max.Y += borderSize;
    min.X -= borderSize;
    min.Y -= borderSize;

    offset.X = - min.X;
    offset.Y = - min.Y;

    imageWidth = max.X + offset.X;
    imageHeight = max.Y + offset.Y;

    imageData = new PixelColor*[imageHeight];
    for (int i = 0; i < imageHeight; ++i)
        imageData[i] = new PixelColor[imageWidth];

    for (auto town : map)
    {
        for (int i = -1; i < 2; i++)
        {
            for (int j = -1; j < 2; j++)
            {
                imageData[town.Y + offset.Y + i][town.X + offset.X + j] = PixelColor(255, 0, 0);
            }
        }
    }

    //DrawLine(Coordinate(69, 28), Coordinate(62, 81));
}

void GraphPainter::MakeRoute(vector<Coordinate> map, vector<int> route)
{
    for (int townNum = 0; townNum < route.size(); townNum++)
    {
        DrawLine(map[route[townNum] - 1], map[route[townNum == route.size() - 1 ? 0 : townNum + 1] - 1]);
    }
}

void GraphPainter::DrawLine(Coordinate p1, Coordinate p2) {

    bool invertXY = false;
    if (abs(p2.X - p1.X) < abs(p2.Y - p1.Y))
    {
        invertXY = true;
        auto temp = p1.X;
        p1.X = p1.Y;
        p1.Y = temp;

        temp = p2.X;
        p2.X = p2.Y;
        p2.Y = temp;
    }

    if (p2.X < p1.X)
    {
        auto temp = p2;
        p2 = p1;
        p1 = temp;
    }

    int dx = p2.X - p1.X;
    int dy = p2.Y - p1.Y;
    for (int x = p1.X; x < p2.X; x++) {
        int y = p1.Y + dy * (x - p1.X) / dx;

        auto pixelData = &(invertXY ? imageData[x + offset.Y][y + offset.X] : imageData[y + offset.Y][x + offset.X]);
        *pixelData = PixelColor(0, 0, 255);

        //if (invertXY)
        //    imageData[x + offset.X][y + offset.Y] = PixelColor(0, 0, 255);
        //else
        //    imageData[y + offset.Y][x + offset.X] = PixelColor(0, 0, 255);
        
    }
}

void GraphPainter::Draw()
{
    ofstream img("picture.ppm");
    img << "P3" << endl;
    img << imageWidth << " " << imageHeight << endl;
    img << "255" << endl;

    imageData[offset.Y][offset.X] = PixelColor(0, 0, 0);

    for (int y = 0; y < imageHeight; y++) {
        for (int x = 0; x < imageWidth; x++) {

            auto p = imageData[y][x];
            img << p.R << " " << p.G << " " << p.B << endl;
        }
    }

    system("cd");
    system("\"D:\\Program Files\\ImageMagick-7.0.8-Q16\\convert.exe\" picture.ppm picture.png");
    //system("picture.png");
    //system("pause");
}

GraphPainter::~GraphPainter()
{
    //Free each sub-array
    for (int i = 0; i < imageHeight; ++i) {
        delete[] imageData[i];
    }
    //Free the array of pointers
    delete[] imageData;
}
