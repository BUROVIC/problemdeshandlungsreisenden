// ProblemDesHandlungsreisenden.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include "allincluded.h"
#include "GraphPainter.h"
#include <cstdlib>
#include <time.h>  
#include <omp.h>
#include <future>
#include <thread>

using namespace std;

extern "C" __declspec(dllexport) void __cdecl FindRoute(int N, int* X, int* Y);
extern "C" __declspec(dllexport) int __cdecl GetCurrentIterationNum();

std::tuple<vector<int>, int> ExactAlgorithm(vector<vector<int>> distancesMatrix);
std::tuple<vector<int>, int> ExactAlgorithmParallel(vector<vector<int>> distancesMatrix);
std::tuple<vector<int>, int> SimulatedAnnealingAlgorithm(vector<vector<int>> distancesMatrix, int shortestDistance);
std::tuple<vector<int>, int> SimulatedAnnealingAlgorithmParallel(vector<vector<int>> distancesMatrix, int shortestDistance);

int Random(int max);
int Random(int min, int max);

int factorial(int n);

template <class Iterator>
bool get_next_permutation(Iterator first, Iterator last);

int currentIterationNum = 0;

//������������ ��������� ��������
bool randomizeData = false;

//���������� ������� � png
bool drawToPng = true;

extern "C" __declspec(dllexport) void __cdecl FindRoute(int N, int* X, int* Y)
{
    srand(time(NULL));  //������������� ���������� ��������� �����

    currentIterationNum = 0;

    vector<Coordinate> coordinates(N);

    cout << "X   Y" << endl;
    for (int i = 0; i < N; i++)
    {
        Coordinate coord(Random(-100, 100), Random(-100, 100));

        if (randomizeData)
        {
            cout << coord.X << " ";
            cout << coord.Y << endl;
        }
        else
        {
            coord.X = X[i];
            coord.Y = Y[i];
        }
        coordinates[i] = coord;
    }

    vector<vector<int>> distancesMatrix(N);
    for (size_t i = 0; i < N; i++)
    {
        vector<int> row(N);
        for (size_t j = 0; j < N; j++)
        {
            if (i != j)
            {
                row[j] = sqrt(pow(coordinates[j].X - coordinates[i].X, 2) + pow(coordinates[j].Y - coordinates[i].Y, 2));
            }
            else
            {
                row[j] = INT_MAX;
            }
        }
        distancesMatrix[i] = row;
    }

    //SOLUTION

    auto[solutionRoute, solutionDistance] = ExactAlgorithm(distancesMatrix);

    //SimulatedAnnealingAlgorithm(distancesMatrix, solutionDistance);

    //SimulatedAnnealingAlgorithmParallel(distancesMatrix, solutionDistance);

    cout << endl << "Solution: " << endl;
    for (auto town : solutionRoute)
    {
        cout << town << " ";
    }
    cout << endl;

    if (drawToPng)
    {
        GraphPainter painter(coordinates);

        painter.MakeRoute(coordinates, solutionRoute);

        painter.Draw();
    }
}

extern "C" __declspec(dllexport) int __cdecl GetCurrentIterationNum()
{
    return currentIterationNum;
}


tuple<vector<int>, int> ExactAlgorithm(vector<vector<int>> distancesMatrix)
{
    vector<int> route;
    int N = distancesMatrix.size();
    for (int i = 0; i < N; i++)
        route.push_back(i + 1);

    tuple<vector<int>, int> shortestRouteAndDistance = { route, INT_MAX };
    do
    {
        currentIterationNum++;
        int distance = 0;
        for (int townNum = 0; townNum < N; townNum++)
            distance += distancesMatrix[route[townNum] - 1][route[townNum == N - 1 ? 0 : townNum + 1] - 1];
        if (distance < get<1>(shortestRouteAndDistance))
            shortestRouteAndDistance = { route, distance };

    } while (get_next_permutation(route.begin(), route.end()));

    return shortestRouteAndDistance;
}

//tuple<vector<int>, int> ExactAlgorithmParallel(vector<vector<int>> distancesMatrix)
//{
//    vector<int> routePermutation;
//    int N = distancesMatrix.size();
//    for (int i = 0; i < N; i++)
//        routePermutation.push_back(i + 1);
//
//
//    tuple<vector<int>, int> shortestRouteAndDistance = { routePermutation, INT_MAX };
//
//    volatile bool flag = false;
//    volatile bool inProcess = false;
//    #pragma omp parallel for shared(flag)
//    for (int i = 0; i < INT_MAX - 1; i++) 
//    {
//        if (flag) continue;
//
//        if (!inProcess)
//        {
//            auto route = routePermutation;
//
//            int distance = 0;
//            for (int townNum = 0; townNum < N; townNum++)
//                distance += distancesMatrix[route[townNum] - 1][route[townNum == N - 1 ? 0 : townNum + 1] - 1];
//            if (distance < get<1>(shortestRouteAndDistance))
//                shortestRouteAndDistance = { route, distance };
//
//            inProcess = true;
//
//            flag = !get_next_permutation(routePermutation.begin(), routePermutation.end());
//
//            inProcess = false;
//        }
//    }
//
//    return shortestRouteAndDistance;
//}

// Function to generate n non-repeating random numbers 
vector<int> generateRandomPermutation(int n)
{
    vector<int> result;
    vector<int> v(n);

    // Fill the vector with the values 
    // 1, 2, 3, ..., n 
    for (int i = 0; i < n; i++)
        v[i] = i + 1;

    // While vector has elements 
    // get a random number from the vector and print it 
    while (v.size()) {
        int n = v.size();

        int index = rand() % n;

        int num = v[index];

        swap(v[index], v[n - 1]);
        v.pop_back();

        result.push_back(num);
    }

    return result;
}

std::tuple<vector<int>, int> SimulatedAnnealingAlgorithm(vector<vector<int>> distancesMatrix, int shortestDistance)
{
    vector<int> route;
    int N = distancesMatrix.size();

    tuple<vector<int>, int> shortestRouteAndDistance = { route, INT_MAX };

    for (;;)
    {
        auto route = generateRandomPermutation(N);

        int distance = 0;
        for (int townNum = 0; townNum < N; townNum++)
            distance += distancesMatrix[route[townNum] - 1][route[townNum == N - 1 ? 0 : townNum + 1] - 1];

        if (distance < get<1>(shortestRouteAndDistance))
            shortestRouteAndDistance = { route, distance };

        if (distance == shortestDistance)
            break;
    }

    return shortestRouteAndDistance;
}

std::tuple<vector<int>, int> SimulatedAnnealingAlgorithmParallel(vector<vector<int>> distancesMatrix, int shortestDistance)
{
    vector<int> route;
    int N = distancesMatrix.size();

    tuple<vector<int>, int> shortestRouteAndDistance = { route, INT_MAX };

    volatile bool flag = false;
    #pragma omp parallel for shared(flag)
    for (int i = 0; i < INT_MAX - 1; i++)
    {
        if (flag) continue;

        auto route = generateRandomPermutation(N);

        int distance = 0;
        for (int townNum = 0; townNum < N; townNum++)
            distance += distancesMatrix[route[townNum] - 1][route[townNum == N - 1 ? 0 : townNum + 1] - 1];

        if (distance < get<1>(shortestRouteAndDistance)){}
            shortestRouteAndDistance = { route, distance };

        if (distance == shortestDistance)
            flag = true;
    }

    return shortestRouteAndDistance;
}


template <class Iterator>
bool get_next_permutation(Iterator first,
    Iterator last) {
    if (first == last) return false;
    Iterator i = first;
    ++i;
    if (i == last) return false;
    i = last;
    --i;

    for (;;) {
        Iterator ii = i--;
        if (*i < *ii) {
            Iterator j = last;
            while (!(*i < *--j));
            iter_swap(i, j);
            reverse(ii, last);
            return true;
        }
        if (i == first) {
            reverse(first, last);
            return false;
        }
    }
}

int factorial(int n)
{
    int i;

    for (i = n - 1; i > 0; i--)
        n *= i;

    return (n == 0) ? 1 : n;
}


int Random(int min, int max)
{
    return rand() % (max - min) + min;
}
int Random(int max)
{
    return Random(0, max);
}