#include "pch.h"
//#include "allincluded.h"


struct Coordinate
{
    int X;
    int Y;

    Coordinate() {
        X = 0;
        Y = 0;
    }

    Coordinate(int x, int y) {
        X = x;
        Y = y;
    }
};

struct PixelColor
{
    int R;
    int G;
    int B;

    PixelColor() {
        R = 255;
        G = 255;
        B = 255;
    }

    PixelColor(int r, int g, int b) {
        R = r;
        G = g;
        B = b;
    }
};

